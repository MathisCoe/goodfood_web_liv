// Système de panier

let localArticles = localStorage.getItem("ArtcilesInCart"); // Articles dans le panier
let allArticles =
    (localArticles !== null) & (localArticles !== "")
        ? JSON.parse(localStorage.getItem("ArtcilesInCart"))
        : [];
let button = document.getElementsByClassName("addToCart");
let numberOfArticleP = document.getElementById("numberOfArt");
let ulCartList = document.getElementById("ArticleListLi");
let total_outside = document.getElementById("numberOfArt");
let removeBtns = document.getElementsByClassName("removeCart");
let spanNum = document.getElementById("spanNum");
let btnCart = document.getElementById("cartNav");
let carItemIdInit =
    (localArticles !== null) & (localArticles !== "")
        ? JSON.parse(localStorage.getItem("ArtcilesInCart")).length
        : 0;
let total =
    localStorage.getItem("Total") !== null &&
    localStorage.getItem("Total") !== NaN
        ? parseFloat(localStorage.getItem("Total"))
        : 0;
 // Quand on ajoute dans le panier       
Array.from(button).forEach(function (element) {
    element.addEventListener("click", (event) => {
        let productImageSrc = element.parentNode.children[0].currentSrc;
        let productName = element.parentNode.children[1].innerText;
        let procutPrice = (element.parentNode.children[2].innerText).slice(0,-1);
        let id = element.parentNode.children[4].innerText;
        let type = element.parentNode.children[5].innerText;
        let description = element.parentNode.children[6].innerText;
        carItemIdInit += 1;
        let newItem = {
            image: productImageSrc,
            name: productName,
            price: procutPrice,
            id: id,
            type: type,
            description: description,
            cartItemId: `cartItem${carItemIdInit}`,
        };
        console.log("newitem", JSON.stringify(newItem, null, 2));
        allArticles.push(newItem);
        localStorage.setItem("ArtcilesInCart", JSON.stringify(allArticles));
        // alert(`Vous venez d'ajouter à votre panier ${newItem.name} à ${newItem.price} €`)

        //Add notification in cart button
        addNotifToCartOrRemove()

        total += parseFloat(procutPrice);
        total_outside.innerText = `${Math.round(total * 100) / 100} €`;
        localStorage.setItem("Total", Math.round(total * 100) / 100);
        let li = document.createElement("li");
        li.setAttribute("class", "flex py-6");
        li.setAttribute("id", `${newItem.cartItemId}`);
        li.innerHTML = `
            <div class="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
                <img src=${newItem.image} alt="" class="h-full w-full object-cover object-center">
            </div>

            <div class="ml-4 flex flex-1 flex-col">
                <div>
                    <div class="flex justify-between text-base font-medium text-gray-900">
                        <h3>
                            <a href="#">${newItem.name} (pour toutes les faims)</a>
                        </h3>
                        <p class="ml-4">${newItem.price}€</p>
                    </div>
                    <p class="mt-1 text-sm text-gray-500">${newItem.description}</p>
                </div>
                <div class="flex flex-1 items-end justify-between text-sm">
                    <p class="text-gray-500"></p>

                    <div class="flex">
                        <button id="${newItem.cartItemId}" type="button" class="font-medium text-indigo-600 hover:text-indigo-500 removeCart">Retirer</button>
                    </div>
                </div>
            </div>
            `;
        ulCartList.appendChild(li);
        removeBtns = document.getElementsByClassName("removeCart");
    });
});
let cleanCartBtn = document.getElementById("cleanCarts");
if (cleanCartBtn !== null) {
    cleanCartBtn.addEventListener("click", (event) => {
        localStorage.removeItem("ArtcilesInCart");
        localStorage.removeItem("Total");
        location.reload();
        console.log("Remove Cart content");
    });
}

let currentArticle = null;

// Quand on retire un article du panier
Array.from(removeBtns).forEach(function (element) {
    element.addEventListener("click", (event) => {
        console.log("ID element", element.id);
        let localAR = JSON.parse(localStorage.getItem("ArtcilesInCart"));
        console.log(localAR);
        currentArticle = localAR.find(
            (elementLoc) => elementLoc.cartItemId === element.id
        );
        let indexOfCuurentArticle = localAR.findIndex(
            (elementLoc) => elementLoc.cartItemId === element.id
        );
        console.log(
            "which item supposed to be removed ",
            indexOfCuurentArticle
        );
        allArticles.splice(indexOfCuurentArticle, 1);
        localStorage.setItem("ArtcilesInCart", JSON.stringify(allArticles));
        total -= parseFloat(currentArticle.price);
        total_final.innerHTML = `${Math.round(total * 100) / 100} €`;
        localStorage.setItem("Total", Math.round(total * 100) / 100);
        document.getElementById(`${element.id}`).remove();
        addNotifToCartOrRemove()
    });
});

function addNotifToCartOrRemove() {
    if(allArticles.length > 0) {
        spanNum.style.display = null
        spanNum.innerText = allArticles.length
        btnCart.classList.add("bg-blue-600");
        btnCart.style.display = "flex"
    } else {
        spanNum.style.display = "none"
        btnCart.classList.remove("bg-blue-600");
    }
}
addNotifToCartOrRemove()

