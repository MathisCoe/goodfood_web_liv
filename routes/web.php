<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    $user = \Auth::user();
    return redirect()->route('carteRestaurant',['idRestaurant' => $user->restaurants_idRestaurant]);
})->middleware(['auth'])->name('dashboard');

Route::get('/restaurant/{idRestaurant}', 'App\Http\Controllers\RestaurantController@carteRestaurant')->name('carteRestaurant');
Route::get('/restaurant', 'App\Http\Controllers\RestaurantController@listeRestaurant')->name('listeRestaurant');
Route::get('/menu/{idMenu}', 'App\Http\Controllers\RestaurantController@detailMenu')->name('detailMenu');
Route::get('/historiqueCommande', 'App\Http\Controllers\UtilisateurController@historiqueCommande')->name('historiqueCommande');
Route::get('/panier', 'App\Http\Controllers\CommandeController@panier')->name('panier');

Route::get('/gestionCommande','App\Http\Controllers\RestaurateurController@gestionCommande')->name('gestionCommande');
Route::post('/terminerCommande','App\Http\Controllers\RestaurateurController@terminerCommande')->name('terminerCommande');
Route::post('/annulerCommande','App\Http\Controllers\RestaurateurController@annulerCommande')->name('annulerCommande');

Route::get('/gestionCarte','App\Http\Controllers\RestaurateurController@gestionCarte')->name('gestionCarte');
Route::post('/creerCommande','App\Http\Controllers\CommandeController@creerCommande')->name('creerCommande');
Route::get('/payerCommande/{idCommande}','App\Http\Controllers\CommandeController@payerCommande')->name('payerCommande');
Route::post('/payerCommande/{idCommande}/final','App\Http\Controllers\CommandeController@payerCommandeFinal')->name('payerCommandeFinal');
Route::get('/commandeReussie','App\Http\Controllers\CommandeController@commandeReussie')->name('commandeReussie');

Route::post('/creerMenu','App\Http\Controllers\RestaurateurController@creerMenu')->name('creerMenu');
Route::post('/creerProduit','App\Http\Controllers\RestaurateurController@creerProduit')->name('creerProduit');
Route::post('/changeDispo','App\Http\Controllers\RestaurateurController@changeDispo')->name('changeDispo');

Route::get('/changeRestaurantFavori/{idRestaurant}','App\Http\Controllers\UtilisateurController@changeRestaurantFavori')->name('changeRestaurantFavori');


require __DIR__.'/auth.php';



// API RESTful
