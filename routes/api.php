<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Models\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/authenticate','App\Http\Controllers\APIController@authenticate')->name('api_menu');


Route::middleware('auth:sanctum')->post('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->post('menu', 'App\Http\Controllers\APIController@menu')->name('api_menu');
Route::middleware('auth:sanctum')->post('produit', 'App\Http\Controllers\APIController@produit')->name('api_produit');

Route::middleware('auth:sanctum')->post('restaurant', 'App\Http\Controllers\APIController@restaurant')->name('user_restaurant');
Route::middleware('auth:sanctum')->post('restaurant/departementUser', 'App\Http\Controllers\APIController@restaurantDepartementUser')->name('restaurantDepartementUser');
Route::middleware('auth:sanctum')->post('restaurant/menus', 'App\Http\Controllers\APIController@restaurantMenus')->name('restaurantMenus');
Route::middleware('auth:sanctum')->post('restaurant/produits', 'App\Http\Controllers\APIController@restaurantProduits')->name('restaurantProduits');

Route::middleware('auth:sanctum')->post('user/commandes', 'App\Http\Controllers\APIController@commandesUser')->name('commandesUser');