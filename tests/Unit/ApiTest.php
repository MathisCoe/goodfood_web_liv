<?php

namespace Tests\Unit;

use Tests\TestCase;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\AssertableJson;
class ApiTest extends TestCase
{

    public function test_menu()
    {
      $user = User::factory()->create([
            'email' => "clientTest2@goodfood.fr",
            'password' => Hash::make('toto'),
            'restaurants_idRestaurant' => 1,
            'grade' => 1
        ]);
      $this->actingAs($user);
      $response =  $this->post('/api/menu', ['idMenu' => '1']);
      $response->assertStatus(200);
      $response
        ->assertJson(fn (AssertableJson $json) =>
            $json->where('success', true)
                 ->etc()
        );
    }

    public function test_menu_non_connecte()
    {
      $response =  $this->post('/api/menu', ['idMenu' => '1']);
      $response->assertStatus(302);
    }
    public function test_produit()
    {
      $user = User::factory()->create([
            'email' => "clientTestProduit@goodfood.fr",
            'password' => Hash::make('toto'),
            'restaurants_idRestaurant' => 1,
            'grade' => 1
        ]);
      $this->actingAs($user);
      $response =  $this->post('/api/produit', ['idProduit' => '1']);
      $response->assertStatus(200);
      $response
        ->assertJson(fn (AssertableJson $json) =>
            $json->where('success', true)
                 ->etc()
        );
    }

    public function test_produit_non_connecte()
    {
      $response =  $this->post('/api/produit', ['idProduit' => '1']);
      $response->assertStatus(302);
    }

    public function test_restaurant()
    {
      $user = User::factory()->create([
            'email' => "clientTest3@goodfood.fr",
            'password' => Hash::make('toto'),
            'restaurants_idRestaurant' => 1,
            'grade' => 1
        ]);
      $this->actingAs($user);
      $response =  $this->post('/api/restaurant', ['idRestaurant' => '1']);
      $response->assertStatus(200);
      $response
        ->assertJson(fn (AssertableJson $json) =>
            $json->where('success', true)
                 ->etc()
        );
    }

    public function test_restaurant_non_connecte()
    {
      $response =  $this->post('/api/restaurant', ['idRestaurant' => '1']);
      $response->assertStatus(302);
    }

    public function test_restaurant_user()
    {
      $user = User::factory()->create([
            'email' => "clientTestRestoUser@goodfood.fr",
            'password' => Hash::make('toto'),
            'restaurants_idRestaurant' => 1,
            'grade' => 1
        ]);
      $this->actingAs($user);
      $response =  $this->post('/api/restaurant/departementUser', []);
      $response->assertStatus(200);
      $response
        ->assertJson(fn (AssertableJson $json) =>
            $json->where('success', true)
                 ->etc()
        );
    }
    public function test_restaurant_user_non_connecte()
    {
      $response =  $this->post('/api/restaurant/departementUser', []);
      $response->assertStatus(302);
    }

    public function test_restaurant_menus()
    {
      $user = User::factory()->create([
            'email' => "clientTestRestoMenus@goodfood.fr",
            'password' => Hash::make('toto'),
            'restaurants_idRestaurant' => 1,
            'grade' => 1
        ]);
      $this->actingAs($user);
      $response =  $this->post('/api/restaurant/menus', ['idRestaurant' => '1']);
      $response->assertStatus(200);
      $response
        ->assertJson(fn (AssertableJson $json) =>
            $json->where('success', true)
                 ->etc()
        );
    }

    public function test_restaurant_menus_non_connecte()
    {
      $response =  $this->post('/api/restaurant/menus', ['idRestaurant' => '1']);
      $response->assertStatus(302);
    }

    public function test_restaurant_produits()
    {
      $user = User::factory()->create([
            'email' => "clientTestRestoProduits@goodfood.fr",
            'password' => Hash::make('toto'),
            'restaurants_idRestaurant' => 1,
            'grade' => 1
        ]);
      $this->actingAs($user);
      $response =  $this->post('/api/restaurant/produits', ['idRestaurant' => '1']);
      $response->assertStatus(200);
      $response
        ->assertJson(fn (AssertableJson $json) =>
            $json->where('success', true)
                 ->etc()
        );
    }

    public function test_restaurant_produits_non_connecte()
    {
      $response =  $this->post('/api/restaurant/produits', ['idRestaurant' => '1']);
      $response->assertStatus(302);
    }

}
