<?php

namespace Tests\Feature;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class RouteTest extends TestCase
{
    public function test_base()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function test_login()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }
    public function test_restaurant()
    {
        $response = $this->get('/restaurant');

        $response->assertStatus(200);
    }
    public function test_menu()
    {
        $response = $this->get('/menu/1');

        $response->assertStatus(200);
    }
    public function test_restaurantChoix()
    {
        $response = $this->get('/restaurant/1');

        $response->assertStatus(200);
    }
    public function test_historiqueCommandes_nonConnecte()
    {
        $response = $this->get('/historiqueCommandes');

        $response->assertStatus(404);
    }
     public function test_historiqueCommandes()
    {
        $user = User::factory()->create([
            'idUtilisateur' => '98',
            'email' => "clientTest@goodfood.fr",
            'password' => Hash::make('toto'),
            'restaurants_idRestaurant' => 1,
            'grade' => 1
        ]);
        $this->actingAs($user);
        $this->assertTrue(\Auth::check());
        $response = $this->get('/historiqueCommandes');

        //$response->assertStatus(200);
      
    }
}
