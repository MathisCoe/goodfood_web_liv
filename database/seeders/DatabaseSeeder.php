<?php

//namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        App\Models\Restaurants::create(
            [
                'idRestaurant' => '1',
                'nomRestaurant' => 'GoodFood du Mans',
                'adresse' => '1 rue bidon 72000 LE MANS',
                'code_postal' => '72',
                'pays' => 'France', 
                'urlPhoto' => '1.png',
                'telephone' => '0222222222'
            ]
          );
          App\Models\Restaurants::create(
            [
                'idRestaurant' => '2',
                'nomRestaurant' => 'GoodFood de La Creuse',
                'adresse' => '1 rue bidon 23200 AUBUSSON',
                'code_postal' => '23',
                'pays' => 'France', 
                'urlPhoto' => '2.png',
                'telephone' => '0222222223'
            ]
          );
          App\Models\Restaurants::create(
            [
                'idRestaurant' => '3',
                'nomRestaurant' => 'Chez Kévin\'',
                'adresse' => '4 rue du port 72000 LE MANS',
                'code_postal' => '72',
                'pays' => 'France', 
                'urlPhoto' => '3.png',
                'telephone' => '0243555555'
            ]
          );
        // Restaurant du Mans
        App\Models\User::create(
            [
              'idUtilisateur' => '1',
              'prenom' => 'Margarita',
              'nom' => 'Le Client',
              'grade' => '1',
              'restaurants_idRestaurant' => '1',
              'code_postal' => '72',
              'pays' => 'France', 
              'email' => 'client@goodfood.fr',
              'password' => Hash::make('toto')
            ]
          );
          App\Models\User::create(
            [
              'idUtilisateur' => '2',
              'prenom' => 'Antonio',
              'nom' => 'Le Kebabier',
              'grade' => '2',
              'restaurants_idRestaurant' => '1',
              'email' => 'restaurateur@goodfood.fr',
              'password' => Hash::make('toto')
            ]
          );
          // Restaurant de la Creuse
          App\Models\User::create(
            [
              'idUtilisateur' => '3',
              'prenom' => 'Pablo',
              'nom' => 'Le Client de La Creuse',
              'grade' => '1',
              'restaurants_idRestaurant' => '2',
              'code_postal' => '23',
              'pays' => 'France', 
              'email' => 'client2@goodfood.fr',
              'password' => Hash::make('toto')
            ]
          );
          App\Models\User::create(
            [
              'idUtilisateur' => '4',
              'prenom' => 'Juan',
              'nom' => 'Le Kebabier de La Creuse',
              'grade' => '2',
              'restaurants_idRestaurant' => '2',
              'email' => 'restaurateur2@goodfood.fr',
              'password' => Hash::make('toto')
            ]
          );
          App\Models\User::create(
            [
              'idUtilisateur' => '5',
              'prenom' => 'Kévin',
              'nom' => 'Le Cuisinier',
              'grade' => '2',
              'restaurants_idRestaurant' => '3',
              'email' => 'restaurateur3@goodfood.fr',
              'password' => Hash::make('toto')
            ]
          );

         
        
          App\Models\CategoriesProduits::create(
            [
                'idCategorieProduit' => '1',
                'nom' => 'Burger'
            ]
          );
          App\Models\CategoriesProduits::create(
            [
                'idCategorieProduit' => '2',
                'nom' => 'Accompagnements'
            ]
          );
          App\Models\CategoriesProduits::create(
            [
                'idCategorieProduit' => '3',
                'nom' => 'Boissons'
            ]
          );
          App\Models\CategoriesProduits::create(
            [
                'idCategorieProduit' => '4',
                'nom' => 'Entrées'
            ]
          );
          App\Models\CategoriesProduits::create(
            [
                'idCategorieProduit' => '5',
                'nom' => 'Plats'
            ]
          );
          App\Models\CategoriesProduits::create(
            [
                'idCategorieProduit' => '6',
                'nom' => 'Desserts'
            ]
          );
          App\Models\Produits::create(
            [
                'idProduit' => '1',
                'nom' => 'Johan Burger',
                'description' => 'Très bon',
                'prix' => '5.90',
                'urlPhoto' => '1.jpg',
                'disponibilite' => '1',
                'idCategorieProduit' => '1',
                'idRestaurant' => '1'

            ]
          );
          App\Models\Produits::create(
            [
                'idProduit' => '2',
                'nom' => 'Frites',
                'description' => 'Très bonne',
                'prix' => '2',
                'urlPhoto' => '2.jpg',
                'disponibilite' => '1',
                'idCategorieProduit' => '2',
                'idRestaurant' => '1'

            ]
          );
          App\Models\Produits::create(
            [
                'idProduit' => '3',
                'nom' => 'Coca cola',
                'description' => 'Très bonne',
                'prix' => '3',
                'urlPhoto' => '3.png',
                'disponibilite' => '1',
                'idCategorieProduit' => '3',
                'idRestaurant' => '1'

            ]
          );
          App\Models\Produits::create(
            [
                'idProduit' => '4',
                'nom' => 'Fanta',
                'description' => 'Très bonne',
                'prix' => '3',
                'urlPhoto' => '4.png',
                'disponibilite' => '1',
                'idCategorieProduit' => '3',
                'idRestaurant' => '1'

            ]
          );
          App\Models\Produits::create(
            [
                'idProduit' => '5',
                'nom' => 'Mathis Burger',
                'description' => 'Très bon',
                'prix' => '5.90',
                'urlPhoto' => '5.jpg',
                'disponibilite' => '1',
                'idCategorieProduit' => '1',
                'idRestaurant' => '1'

            ]
          );
          App\Models\Produits::create(
            [
                'idProduit' => '6',
                'nom' => 'Viande de Kobe',
                'description' => 'Très bon',
                'prix' => '80',
                'urlPhoto' => '6.jpg',
                'disponibilite' => '1',
                'idCategorieProduit' => '5',
                'idRestaurant' => '3'

            ]
          );
          App\Models\Produits::create(
            [
                'idProduit' => '7',
                'nom' => 'Huitre',
                'description' => 'Très bon',
                'prix' => '10',
                'urlPhoto' => '7.jpg',
                'disponibilite' => '1',
                'idCategorieProduit' => '4',
                'idRestaurant' => '3'

            ]
          );
          App\Models\Produits::create(
            [
                'idProduit' => '8',
                'nom' => 'Vin',
                'description' => 'Très bon',
                'prix' => '30',
                'urlPhoto' => '8.jpg',
                'disponibilite' => '1',
                'idCategorieProduit' => '6',
                'idRestaurant' => '3'

            ]
          );
          App\Models\Menus::create(
            [
                'idMenu' => '1',
                'nomMenu' => 'Johan\'Box',
                'descriptionMenu' => 'Un menu adapté à toutes les faims !',
                'urlPhoto' => '1.png',
                'prix' => '12.90',
                'idRestaurant' => '1',
                'disponibilite' => '1'
            ]
          );
          App\Models\MenusHasProduits::create(
            [
                'idMenu' => '1', // Johan'Box
                'idProduit' => '1' // Johan Burger
            ]
          );
          App\Models\MenusHasProduits::create(
            [
                'idMenu' => '1', // Johan'Box
                'idProduit' => '2' // Frites
            ]
          );
          App\Models\MenusHasProduits::create(
            [
                'idMenu' => '1', // Johan'Box
                'idProduit' => '3' // Coca Cola
            ]
          );
          App\Models\MenusHasProduits::create(
            [
                'idMenu' => '1', // Johan'Box
                'idProduit' => '4' // Fanta
            ]
          );
    
          App\Models\Menus::create(
            [
                'idMenu' => '2',
                'nomMenu' => 'Mega Mathis',
                'descriptionMenu' => 'Un menu d\'exception !',
                'urlPhoto' => '2.png',
                'prix' => '16.90',
                'idRestaurant' => '1',
                'disponibilite' => '1'
            ]
          );
          App\Models\MenusHasProduits::create(
            [
                'idMenu' => '2', // Mega Mathis
                'idProduit' => '5' // Johan Burger
            ]
          );
          App\Models\MenusHasProduits::create(
            [
                'idMenu' => '2', // Mega Mathis
                'idProduit' => '2' // Frites
            ]
          );
          App\Models\MenusHasProduits::create(
            [
                'idMenu' => '2', // Mega Mathis
                'idProduit' => '3' // Coca Cola
            ]
          );
          App\Models\MenusHasProduits::create(
            [
                'idMenu' => '2', // Mega Mathis
                'idProduit' => '4' // Fanta
            ]
          );
          App\Models\Commandes::create(
            [
                'idRestaurant' => '1', // GoodFood Le Mans
                'idUtilisateur' => '1', // Margarita
                'idCommande' => '1',
                'etatCommande' => '1' // En Cours
            ]
          );
          App\Models\CommandesHasMenus::create(
            [
                'idCommande' => '1', 
                'idMenu' => '1',// Johan'Box
                'quantite' => '1',
                'prix' => '12.9'
            ]
          );
          App\Models\Commandes::create(
            [
                'idRestaurant' => '1', // GoodFood Le Mans
                'idUtilisateur' => '1', // Margarita
                'idCommande' => '2',
                'etatCommande' => '1' // En Cours
            ]
          );
          App\Models\CommandesHasProduits::create(
            [
                'idCommande' => '2', 
                'idProduit' => '1',// 
                'quantite' => '1',
                'prix' => '3'
            ]
          );
          App\Models\CommandesHasProduits::create(
            [
                'idCommande' => '2', 
                'idProduit' => '2',// 
                'quantite' => '2',
                'prix' => '2'
            ]
          );
    }
}
