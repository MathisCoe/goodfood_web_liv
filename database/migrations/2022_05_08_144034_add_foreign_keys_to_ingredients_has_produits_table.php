<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToIngredientsHasProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ingredients_has_produits', function (Blueprint $table) {
            $table->foreign(['idIngredient'], 'fk_ingredients_has_produits_ingredients1')->references(['idIngredient'])->on('ingredients')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['idProduit'], 'fk_ingredients_has_produits_produits1')->references(['idProduit'])->on('produits')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ingredients_has_produits', function (Blueprint $table) {
            $table->dropForeign('fk_ingredients_has_produits_ingredients1');
            $table->dropForeign('fk_ingredients_has_produits_produits1');
        });
    }
}
