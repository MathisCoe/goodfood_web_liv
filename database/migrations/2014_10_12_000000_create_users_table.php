<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('idUtilisateur',true);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('prenom', 45)->nullable();
            $table->string('nom', 45)->nullable();
            $table->string('pays', 45)->nullable();
            $table->string('code_postal', 45)->nullable();
            $table->string('telephone', 45)->nullable();
            $table->integer('grade')->nullable();
            $table->integer('restaurants_idRestaurant')->index('fk_users_restaurants1_idx');
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
