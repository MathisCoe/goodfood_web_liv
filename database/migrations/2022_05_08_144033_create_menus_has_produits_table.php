<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusHasProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus_has_produits', function (Blueprint $table) {
            $table->integer('idMenu')->index('fk_menus_has_produits_menus1_idx');
            $table->integer('idProduit')->index('fk_menus_has_produits_produits1_idx');

            $table->primary(['idMenu', 'idProduit']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus_has_produits');
    }
}
