<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produits', function (Blueprint $table) {
            $table->foreign(['idCategorieProduit'], 'fk_produits_categories_produits1')->references(['idCategorieProduit'])->on('categories_produits')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['idRestaurant'], 'fk_produits_has_restaurants_restaurants1')->references(['idRestaurant'])->on('restaurants')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produits', function (Blueprint $table) {
            $table->dropForeign('fk_produits_categories_produits1');
            $table->dropForeign('fk_produits_has_restaurants_restaurants1');
        });
    }
}
