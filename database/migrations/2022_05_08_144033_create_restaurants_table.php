<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->integer('idRestaurant',true)->unique('idRestaurant_UNIQUE');
            $table->string('nomRestaurant', 45);
            $table->string('urlPhoto', 255)->nullable();
            $table->string('adresse', 45)->nullable();
            $table->string('pays', 45)->nullable();
            $table->string('code_postal', 45)->nullable();
            $table->string('telephone', 45)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
