<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->integer('idProduit',true)->unique('idProduit_UNIQUE');
            $table->string('nom', 45);
            $table->string('description', 45)->nullable();
            $table->float('prix', 10, 0);
            $table->string('urlPhoto', 255)->nullable();
            $table->tinyInteger('disponibilite')->nullable();
            $table->integer('idCategorieProduit')->index('fk_produits_categories_produits1_idx');
            $table->integer('idRestaurant')->index('fk_produits_has_restaurants_restaurants1_idx');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produits');
    }
}
