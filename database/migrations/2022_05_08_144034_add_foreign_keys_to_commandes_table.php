<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commandes', function (Blueprint $table) {
            $table->foreign(['idUtilisateur'], 'fk_commandes_utilisateurs')->references(['idUtilisateur'])->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['idRestaurant'], 'fk_commandes_restaurant')->references(['idRestaurant'])->on('restaurants')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commandes', function (Blueprint $table) {
            $table->dropForeign('fk_commandes_utilisateurs');
            $table->dropForeign('fk_commandes_restaurant');
        });
    }
}
