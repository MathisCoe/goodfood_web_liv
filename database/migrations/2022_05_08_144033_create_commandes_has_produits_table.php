<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandesHasProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes_has_produits', function (Blueprint $table) {
            $table->integer('idCommande')->index('fk_commandes_has_produits_commandes1_idx');
            $table->integer('idProduit')->index('fk_commandes_has_produits_produits1_idx');
            $table->integer('quantite');
            $table->float('prix');

            $table->primary(['idCommande', 'idProduit']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes_has_produits');
    }
}
