<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToMenusHasProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menus_has_produits', function (Blueprint $table) {
            $table->foreign(['idMenu'], 'fk_menus_has_produits_menus1')->references(['idMenu'])->on('menus')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['idProduit'], 'fk_menus_has_produits_produits1')->references(['idProduit'])->on('produits')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menus_has_produits', function (Blueprint $table) {
            $table->dropForeign('fk_menus_has_produits_menus1');
            $table->dropForeign('fk_menus_has_produits_produits1');
        });
    }
}
