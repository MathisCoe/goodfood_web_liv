<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToCommandesHasMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commandes_has_menus', function (Blueprint $table) {
            $table->foreign(['idCommande'], 'fk_commandes_has_menus_commandes1')->references(['idCommande'])->on('commandes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['idMenu'], 'fk_commandes_has_menus_menus1')->references(['idMenu'])->on('menus')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commandes_has_menus', function (Blueprint $table) {
            $table->dropForeign('fk_commandes_has_menus_commandes1');
            $table->dropForeign('fk_commandes_has_menus_menus1');
        });
    }
}
