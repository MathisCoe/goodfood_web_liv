<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandesHasMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes_has_menus', function (Blueprint $table) {
            $table->integer('idCommande')->index('fk_commandes_has_menus_commandes1_idx');
            $table->integer('idMenu')->index('fk_commandes_has_menus_menus1_idx');
            $table->integer('quantite');
            $table->float('prix');
            
            $table->primary(['idCommande', 'idMenu']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes_has_menus');
    }
}
