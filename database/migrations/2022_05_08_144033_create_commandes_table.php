<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->integer('idCommande',true)->unique('idCommande_UNIQUE');
            $table->integer('idUtilisateur')->index('fk_commandes_utilisateurs_idx');
            $table->integer('idRestaurant')->index('fk_commandes_restaurant_idx');
            $table->integer('etatCommande');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes');
    }
}
