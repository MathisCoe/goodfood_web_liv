<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->integer('idMenu',true);
            $table->string('nomMenu', 45);
            $table->string('descriptionMenu', 255);
            $table->string('urlPhoto', 255)->nullable();
            $table->float('prix', 8, 2);
            $table->integer('idRestaurant')->index('fk_menus_has_restaurants_restaurants1_idx');
            $table->boolean('disponibilite')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
