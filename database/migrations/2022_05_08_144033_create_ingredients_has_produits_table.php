<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientsHasProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients_has_produits', function (Blueprint $table) {
            $table->integer('idIngredient')->index('fk_ingredients_has_produits_ingredients1_idx');
            $table->integer('idProduit')->index('fk_ingredients_has_produits_produits1_idx');

            $table->primary(['idIngredient', 'idProduit']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredients_has_produits');
    }
}
