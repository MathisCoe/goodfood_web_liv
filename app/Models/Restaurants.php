<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurants extends Model
{
    protected $table = 'restaurants';
    protected $primaryKey = 'idRestaurant';
    public $timestamps = false;
    use HasFactory;
    
    public function users(){
        return $this->hasMany(User::class,'idUtilisateur'); # 0.n
    }
}
