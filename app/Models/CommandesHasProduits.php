<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommandesHasProduits extends Model
{
    use HasFactory;
    protected $table = 'commandes_has_produits';
    public $timestamps = false;
}
