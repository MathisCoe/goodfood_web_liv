<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredients extends Model
{
    use HasFactory;
    protected $table = 'ingredients';
    protected $primaryKey = 'idIngredient';
    public $timestamps = false;
    
    public function produits(){
        return $this->belongsToMany(Produits::class)->using(IngredientsHasProduits::class); // n.n
    }
}
