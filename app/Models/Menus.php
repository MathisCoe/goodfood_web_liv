<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menus extends Model
{
    use HasFactory;
    protected $table = 'menus';
    protected $primaryKey = 'idMenu';
    public $timestamps = false;
    
    public function produits(){
     
        $listeIdProduits = \App\Models\MenusHasProduits::where("idMenu","=",$this->idMenu)->get()->toArray();
        $produits = collect();
        foreach ($listeIdProduits as $id) {
            $produit = \App\Models\Produits::find($id['idProduit']);
            $produits->push($produit);
        }
        return $produits;
    }
}
