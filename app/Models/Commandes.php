<?php

namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commandes extends Model
{
    use HasFactory;
    protected $table = 'commandes';
    protected $primaryKey = 'idCommande';
   // public $timestamps = false;
    public function user(){
        return $this->belongsTo(User::class,'idUtilisateur'); # 0.n
    }

    public function produits(){
     
        $listeIdProduits = \App\Models\CommandesHasProduits::where("idCommande","=",$this->idCommande)->get()->toArray();
        $produits = collect();
        foreach ($listeIdProduits as $id) {
            $produit = \App\Models\Produits::find($id['idProduit']);
            $produits->push($produit);
        }
        return $produits;
    }

    public function menus(){
        $listeIdMenu = \App\Models\CommandesHasMenus::where("idCommande","=",$this->idCommande)->get()->toArray();
        $menus = collect();
        foreach ($listeIdMenu as $id) {
            $menu = \App\Models\Menus::find($id['idMenu']);
            $menus->push($menu);
        }
        return $menus;
    }

    public function quantiteMenu($idMenu){
        $join = \App\Models\CommandesHasMenus::where("idCommande","=",$this->idCommande)->where("idMenu","=",$idMenu)->get();
        return $join[0]->quantite;
    }
    
    public function quantiteProduit($idProduit){
        $join = \App\Models\CommandesHasProduits::where("idCommande","=",$this->idCommande)->where("idProduit","=",$idProduit)->get();
        return $join[0]->quantite;
    }

    public function date() {
        $date  = Carbon::createFromFormat('Y-m-d H:i:s',$this->created_at)->format('d-m-Y à H:i');
        return $date;
      }
}
