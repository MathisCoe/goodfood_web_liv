<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoriesProduits extends Model
{
    protected $table = 'categories_produits';
    protected $primaryKey = 'idCategorieProduit';
    public $timestamps = false;
    use HasFactory;
      
      public function produits(){
        return $this->hasMany(Produits::class,'idProduit'); # 0.n
      }

      public function isInRestaurant($idRestaurant){
        $isInRestaurant = true;
        $produits = \App\Models\Produits::where("idRestaurant","=",$idRestaurant)->where("idCategorieProduit","=",$this->idCategorieProduit)->get()->toArray();
        if(count($produits) == 0){
          $isInRestaurant = false;
        }
        return $isInRestaurant;
      }
      
      public function isInMenu($idMenu){
        $isInMenu = true;
        $compteur = 0;
        $menu = Menus::find($idMenu)->first();
        $produits = $menu->produits();
        foreach ($produits as $key => $produit) {
          if($produit['idCategorieProduit'] == $this->idCategorieProduit){
            $compteur = $compteur + 1;
          }
        }
        if($compteur == 0){
          $isInMenu = false;
        }
        return $isInMenu;
      }
  
}
