<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommandesHasMenus extends Model
{
    use HasFactory;
    protected $table = 'commandes_has_menus';
    public $timestamps = false;
}
