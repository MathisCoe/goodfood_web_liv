<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenusHasProduits extends Model
{
    use HasFactory;
    protected $table = 'menus_has_produits';
    public $timestamps = false;
}
