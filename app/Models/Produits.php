<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produits extends Model
{
    use HasFactory;
    protected $table = 'produits';
    protected $primaryKey = 'idProduit';
    public $timestamps = false;
    
    public function categoriesProduits(){
        return $this->belongsTo(CategoriesProduits::class,'categories_produits_idCategorieProduit'); # 0.n
    }
    public function restaurants(){
        return $this->belongsToMany(Restaurants::class)->using("idRestaurant"); // n.n
    }
}
