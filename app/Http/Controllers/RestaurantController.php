<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
  // get('/restaurant/{idRestaurant}')->name('carteRestaurant')
  public function carteRestaurant(Request $request)
  {
    $menus = \App\Models\Menus::where("idRestaurant","=",$request->idRestaurant)->get();
    $produits = \App\Models\Produits::where("idRestaurant","=",$request->idRestaurant)->get();
    $categoriesProduits = \App\Models\CategoriesProduits::all();
    $restaurant = \App\Models\Restaurants::find($request->idRestaurant);
    return view('menuRestaurant')->with('produits', $produits->toArray())->with('menus', $menus->toArray())->with('categoriesProduits',$categoriesProduits)->with('restaurant',$restaurant);
  }

  // get('/restaurant')->name('listeRestaurant')
  public function listeRestaurant(Request $request)
  {
    if (\Auth::user()) {
        $restaurants = \App\Models\Restaurants::where("code_postal","=",\Auth::user()->code_postal)->get()->toArray();
    }else{
        $restaurants = \App\Models\Restaurants::all();
    }
    return view('listeRestaurant')->with('restaurants', $restaurants);
  }

  
  // get('/menu/{idMenu}')->name('detailMenu');
  public function detailMenu(Request $request)
  {
    $menu = \App\Models\Menus::find($request->idMenu);
    $listeIdProduits = \App\Models\MenusHasProduits::where("idMenu","=",$request->idMenu)->get();
    $produits = collect();
    foreach ($listeIdProduits as $id) { // On récupère chaque produit
        $produit = \App\Models\Produits::find($id->idProduit);
        $produits->push($produit);
    }
    $categoriesProduits = \App\Models\CategoriesProduits::all();
    return view('detailMenu')->with('menu', $menu)->with('produits', $produits->toArray())->with('categoriesProduits',$categoriesProduits);
  }

}

// Vous regardez le code source ? 
