<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommandeController extends Controller
{
  // get('/panier')->name('panier')
  public function panier(Request $request){
    return view('client.panier');
  }
 
  // post('/creerCommande')->name('creerCommande')
  public function creerCommande(Request $request){
    $commande = new \App\Models\Commandes;
    $commande->idUtilisateur = \Auth::user()->idUtilisateur;
    $commande->idRestaurant = 1; 
    $commande->etatCommande = 4;
    $commande->save();
    $id= $commande->idCommande;
    $nbArticle = count($request->id);
    $prixTotal = 0;
    for ($i=1; $i < $nbArticle+1; $i++) { 
      if($request->type[$i] == "menu"){
        $article = new \App\Models\CommandesHasMenus;
        $article->idMenu = $request->id[$i];
      }elseif ($request->type[$i] == "produit") {
        $article = new \App\Models\CommandesHasProduits;
        $article->idProduit = $request->id[$i];
      }
       $article->prix = $request->prix[$i];
       $article->quantite = 1;
       $article->idCommande = $id;
       $article->save();

       $prixTotal = $prixTotal + $request->prix[$i];
     }
    return redirect()->route('payerCommande', ['idCommande'=>$id]);
  }

  // get('/payerCommande')->name('payerCommande')
  public function payerCommande(Request $request){
    $commande = \App\Models\Commandes::find($request->idCommande);
    $prixTotal = 0;
    $menus = \App\Models\CommandesHasMenus::where('idCommande','=',$request->idCommande)->get();
    foreach ($menus as $menu) {
      $prixTotal = $prixTotal + $menu->prix;
    }
    $produits = \App\Models\CommandesHasProduits::where('idCommande','=',$request->idCommande)->get();
    foreach ($produits as $produit) {
      $prixTotal = $prixTotal + $produit->prix;
    }
    return view('client.payerCommande')->with("idCommande",$request->idCommande)->with('prixTotal',$prixTotal);
  }

  // post('/payerCommande/{idCommande}/final')->name('payerCommandeFinal');
  public function payerCommandeFinal(Request $request){
    // traitement bouchonné 
    $commande = \App\Models\Commandes::find($request->idCommande);
    $commande->etatCommande = 1; // En Cours
    $commande->save();
    return redirect()->route('commandeReussie');
  }
  
  // get('/commandeReussie')->name('commandeReussie');
  public function commandeReussie(Request $request){
    return view('client.commandeReussie');
  }

}
