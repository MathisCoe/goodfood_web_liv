<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\User;
use Illuminate\Support\Facades\Hash;
// Classe contenant l'API Restful

class APIController extends Controller
{
  
  public function authenticate(Request $request) {
    $user = User::where('email' , $request->email)->where('password' ,  Hash::make($request->password))->first();
    $user = User::where('email' , $request->email)->first();
    $token = $user->createToken("test");
    return ['token' => $token->plainTextToken];
  }

  // get('api/menu/{idMenu}')->name('');
  public function menu(Request $request)
  {
    $menu = \App\Models\Menus::find($request->idMenu);
    $listeIdProduits = \App\Models\MenusHasProduits::where("idMenu","=",$request->idMenu)->get();
    $produits = collect();
    foreach ($listeIdProduits as $id) {
        $produit = \App\Models\Produits::find($id->idProduit);
        $produits->push($produit);
    }
    return response()->json(['success' => true, 'menu' => $menu, 'produits' => $produits]);
  }
  
  // get('api/produit/{idProduit}')->name('');
  public function produit(Request $request)
  {
    $produit = \App\Models\Produits::find($request->idProduit);
    return response()->json(['success' => true, 'produit' => $produit]);
  }

  // get('api/restaurant')->name('');
  public function restaurant(Request $request)
  {
    $restaurant = \App\Models\Restaurants::find($request->idRestaurant);
    return response()->json(['success' => true, 'restaurant' => $restaurant]);
  }

  // get('api/restaurant/departementUser')->name('');
  public function restaurantDepartementUser(Request $request)
  {
    $user = $request->user();
    $restaurants = \App\Models\Restaurants::where("code_postal","=",$user->code_postal)->get();
    return response()->json(['success' => true, 'restaurants' => $restaurants,'nbRestaurants' => count($restaurants)]);
  }

  // get('api/restaurant/menus')->name('restaurantMenus');
  public function restaurantMenus(Request $request)
  {
    $menus = \App\Models\Menus::where("idRestaurant","=",$request->idRestaurant)->get();
    foreach ($menus as $key => $menu) {
      $menu->setAttribute('produits', $menu->produits());
    }
    return response()->json(['success' => true, 'menus' => $menus,'nbMenus' => count($menus)]);
  }
  
  // get('api/restaurant/produits')->name('restaurantProduits');
  public function restaurantProduits(Request $request)
  {
    $produits = \App\Models\Produits::where("idRestaurant","=",$request->idRestaurant)->get();
    return response()->json(['success' => true, 'produits' => $produits,'nbProduits' => count($produits)]);
  }
  
  // get('api/user/commandes')->name('commandesUser');
  public function commandesUser(Request $request)
  {
    $user = $request->user();
    $commandes = \App\Models\Commandes::where("idUtilisateur","=",$user->idUtilisateur)->orderBy('idCommande', 'desc')->get();
    foreach ($commandes as $key => $commande) {
      $commande->setAttribute('produits', $commande->produits());
      $commande->setAttribute('menus', $commande->menus());
    }
    return response()->json(['success' => true, 'commandes' => $commandes,'nbCommandes' => count($commandes)]);
  }

}

