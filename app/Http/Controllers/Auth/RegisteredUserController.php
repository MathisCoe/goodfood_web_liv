<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Restaurants;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $restaurants = \App\Models\Restaurants::all();
        return view('auth.register')->with('restaurants',$restaurants);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
      /*  $request->validate([
            'prenomUtilisateur' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);
      */
        $utilisateur = New User;
        $utilisateur->nom = htmlspecialchars(strtoupper($request->nomUtilisateur));
        $utilisateur->prenom = htmlspecialchars(ucfirst($request->prenomUtilisateur));
        $utilisateur->email = $request->email;
        $utilisateur->restaurants_idRestaurant = $request->idRestaurant;
        $utilisateur->password = Hash::make($request->password);
        $utilisateur->grade = 1;
        $restaurant = Restaurants::find($request->idRestaurant)->first();
        $utilisateur->code_postal = $restaurant->code_postal;
        $utilisateur->save();
        event(new Registered($utilisateur));

        Auth::login($utilisateur);

        return redirect(RouteServiceProvider::HOME);
    }
}
