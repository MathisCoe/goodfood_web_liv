<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UtilisateurController extends Controller
{
  // get('/historiqueCommande')->name('historiqueCommande')
  public function historiqueCommande(Request $request){
    if(\Auth::user()){
      $commandes = \App\Models\Commandes::where("idUtilisateur","=",\Auth::user()->idUtilisateur)->orderBy('idCommande', 'desc')->get();
      return view('client.historiqueCommande')->with('commandes', $commandes);
    }else{
      abort(404);
    }
  }

  // post('/changeRestaurantFavori')->name('changeRestaurantFavori');
  public function changeRestaurantFavori(Request $request){
    $user = \Auth::user();
    $user->restaurants_idRestaurant = $request->idRestaurant;
    $user->save();
    return redirect()->route('carteRestaurant',['idRestaurant' => $request->idRestaurant]);
  }
}
