<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RestaurateurController extends Controller
{
  // get('/gestionCommande')->name('gestionCommande')
  public function gestionCommande(Request $request){
    $commandes = \App\Models\Commandes::where("idRestaurant","=",\Auth::user()->restaurants_idRestaurant)->get();
    return view('restaurateur.gestionCommande')->with('commandes', $commandes);
  }

  public function terminerCommande(Request $request){
    $commande = \App\Models\Commandes::find($request->idCommande);
    $commande->etatCommande = 2; // Terminé
    $commande->save();
    return redirect()->route('gestionCommande');
  }

  public function annulerCommande(Request $request){
    $commande = \App\Models\Commandes::find($request->idCommande);
    $commande->etatCommande = 3; // Annulé
    $commande->save();
    return redirect()->route('gestionCommande');
  }

  // get('/gestionCarte')->name('gestionCarte')
  public function gestionCarte(Request $request){
    $menus = \App\Models\Menus::where("idRestaurant","=",\Auth::user()->restaurants_idRestaurant)->get();
    $produits = \App\Models\Produits::where("idRestaurant","=",\Auth::user()->restaurants_idRestaurant)->get();
    $categoriesProduits = \App\Models\CategoriesProduits::all();
    return view('restaurateur.gestionCarte')->with('menus', $menus)->with('produits', $produits)->with('categoriesProduits',$categoriesProduits)->with('idRestaurant',\Auth::user()->restaurants_idRestaurant);
  }

  // get('/creerProduit')->name('creerProduit')
  public function creerProduit(Request $request){
    $produit = new \App\Models\Produits;
    $produit->nom = $request->nom;
    $produit->description = $request->description;
    $produit->prix = $request->prix;
    $produit->idCategorieProduit = $request->idCategorieProduit;
    $produit->idRestaurant = \Auth::user()->restaurants_idRestaurant;
    $produit->save();
    $id= $produit->idProduit;
    $file = $request->file('img'); // on recupère l'image brute
    if($file != null){ // Si le gestionnaire a bien mis une image
      $extension = $file->extension(); // son extension
      $nomImage = $id.'.'.$extension; // Son nom
      $destinationPath = 'images/produits/'; // Sa destination
      $file->move($destinationPath, $nomImage); // on déplace l'image
      $produit->urlPhoto = $nomImage;
    }
    $produit->save();
    return redirect()->route('gestionCarte');
  }
    
  // post('/creerMenu')->name('creerMenu')
  public function creerMenu(Request $request){
      if($request->produits != null){
        $menu = new \App\Models\Menus;
        $menu->nomMenu = $request->nom;
        $menu->descriptionMenu = $request->description;
        $menu->prix = $request->prix;
        $menu->idRestaurant = \Auth::user()->restaurants_idRestaurant;
        $menu->save();
        $id= $menu->idMenu;
        $file = $request->file('img'); // on recupère l'image brute
        if($file != null){ // Si le restaurateur a bien mis une image
          $extension = $file->extension(); // son extension
          $nomImage = $id.'.'.$extension; // Son nom
          $destinationPath = 'images/menus/'; // Sa destination
          $file->move($destinationPath, $nomImage); // on déplace l'image
          $menu->urlPhoto = $nomImage;
        }
        $menu->save();
        foreach ($request->produits as $key => $idProduit) {
          $objet = new \App\Models\MenusHasProduits;
          $objet->idProduit = $idProduit;
          $objet->idMenu = $id;
          $objet->save();
        }
      }
      return redirect()->route('gestionCarte');
  }

  // post('/changeDispo')->name('changeDispo');
  public function changeDispo(Request $request){
      if ($request->idMenu != null) {
        $objet = \App\Models\Menus::find($request->idMenu);
      }
      if ($request->idProduit != null) {
        $objet = \App\Models\Produits::find($request->idProduit);
      }
      if($request->dispo == "1"){ 
        $objet->disponibilite = 1;// Disponible
      }
      elseif($request->dispo == "2"){ 
        $objet->disponibilite = 2; // Indisponible 
      }else{
        $objet->disponibilite = 0; // En Rupture de Stock
      }
      $objet->save();
      return redirect()->route('gestionCarte');
    }
}
