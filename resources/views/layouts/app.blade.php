<!DOCTYPE html>
<html>

<head>
    <title>GoodFood - @yield('title')</title>
    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="{{ asset('images/goodfood.png') }}" />
    <link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdn.tailwindcss.com"></script>
    <script>
        tailwind.config = {
            theme: {
                extend: {
                    colors: {
                        laravel: "#ef3b2d",
                    },
                },
            },
        };
    </script>
</head>

<body class="">
    <nav class="flex justify-between items-center mb-4">
        <a class="navbar-brand" href="/"><img class="w-28 ml-4 rounded-lg" src="/images/goodfood.png"></img></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- -->
        <ul class="flex space-x-6 mr-6 text-lg">
            <!-- Navigation des clients -->
            @if (Auth::user() && Auth::user()->grade == 1)
            <li>
                <a href="/restaurant/{{Auth::user()->restaurant->idRestaurant}}" class="hover:text-laravel">{{Auth::user()->restaurant->nomRestaurant}}</a>
            </li>
            <li>
                <a href="/restaurant" class="hover:text-laravel">Liste des Restaurants</a>
            </li>
            <li class="group relative dropdown">
                <a class="hover:text-laravel" role="button" data-bs-toggle="dropdown" aria-expanded="false">{{Auth::user()->prenom}} {{Auth::user()->nom}}</a>
                <div class="group-hover:block dropdown-menu">
                <ul class="top-0 block " aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Profil</a></li>
                    <li><a class="dropdown-item" href="/historiqueCommande">Historique des Commandes</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="{{route('logout')}}"> Déconnexion</a></li>
                </ul>
                </div>
            </li>
            <li class="">
            <div class="flex space-x-2 justify-center">
                <button id="cartNav" class="inline-block px-6 py-2.5 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-cyan-500 hover:shadow-lg focus:shadow-lg  focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
                    <img src="/images/panier.svg"> </img>
                    <span id="spanNum"  class="inline-block py-1 px-1.5 leading-none text-center whitespace-nowrap align-baseline font-bold bg-red-600 text-white rounded ml-2"></span>
                </button>
            </div>
                @include('partials._shopping_cart')
            </li>
            @elseif(Auth::user() && Auth::user()->grade == 2)
            <li>
                <a href="/gestionCommande" class="hover:text-laravel">Gestion des commandes</a>
                <a href="/gestionCarte" class="hover:text-laravel">Gestion de la carte</a>
            </li>
            <li class="group relative dropdown">
                <a class="hover:text-laravel" role="button" data-bs-toggle="dropdown" aria-expanded="false">{{Auth::user()->prenom}} {{Auth::user()->nom}}</a>
                <div class="group-hover:block dropdown-menu">
                <ul class="top-0 block " aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Profil</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="{{route('logout')}}"> Déconnexion</a></li>
                </ul>
                </div>
            </li>
            @else
            <li>
                <a href="/restaurant" class="hover:text-laravel">Liste des Restaurants</a>
            </li>
            <li>
              <a href="{{route('login')}}" class="hover:text-laravel"><i class="fa-solid fa-arrow-right-to-bracket"></i> Connexion</a>
              <a href="{{route('register')}}" class="hover:text-laravel"><i class="fa-solid fa-user-plus"></i> Inscription</a>
            </li>
            @endif
        </ul>
    </nav>

    @show
    <main>
        <div class="mb-48 container">
            @yield('content')

        </div>
        <button type="button" data-mdb-ripple="true" data-mdb-ripple-color="light" class=" animate-bounce fixed bottom-0 inline-block p-3 bg-sky-500 text-white font-medium text-xs leading-tight uppercase rounded-full shadow-md hover:bg-red-700 hover:shadow-lg focus:bg-red-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-red-800 active:shadow-lg transition duration-150 ease-in-out bottom-5 right-5" id="btn-back-to-top">
        <svg aria-hidden="true" focusable="false" data-prefix="fas" class="w-4 h-4" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
            <path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path>
        </svg>
        </button>
    </main>
    @include('components.footer')

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- <script src="{{ asset('/js/panier.js') }}"></script> -->
    <script src="{{ asset('/js/getBackOnTop.js') }}"></script>
    <script src="{{ asset('/js/cartManage.js') }}"></script>
    @yield('modals')
</body>

</html>
