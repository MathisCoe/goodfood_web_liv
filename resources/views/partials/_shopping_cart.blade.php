 <div class="offcanvas offcanvas-end pointer-events-auto w-screen max-w-md transition duration-300 ease-in-out text-gray-700 top-0 w-auto" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
     <div class="flex h-full flex-col bg-white shadow-xl">
         <div class="flex-1 overflow-y-auto py-6 px-4 sm:px-6">
             <div class="flex items-start justify-between">
                 <h2 class="text-lg font-medium text-gray-900" id="slide-over-title">Panier</h2>
                 <div class="ml-3 flex h-7 items-center">
                     <button type="button" class="-m-2 p-2 text-gray-400 hover:text-gray-500" data-bs-dismiss="offcanvas">
                         <span class="sr-only">Close panel</span>
                         <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" aria-hidden="true">
                             <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                         </svg>
                     </button>
                 </div>
             </div>

             <div class="mt-8">
                 <div class="flow-root">
                     <ul id="ArticleListLi" role="list" class="-my-6 divide-y divide-gray-200">

                 </div>
             </div>
         </div>

         <div class="border-t border-gray-200 py-6 px-4 sm:px-6">
             <div class="flex justify-between text-base font-medium text-gray-900">
                 <p>Total</p>
                 <p id="numberOfArt"> panier vide :(</p>
             </div>
             <div class="mt-6">
                 <a href="/panier" class="flex items-center justify-center rounded-md border border-transparent bg-cyan-500 px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-indigo-700">Voir le panier</a>
             </div>
             <!-- <div class="mt-6 flex justify-center text-center text-sm text-gray-500">
                 <p>
                     ou <button type="button" class="font-medium text-bg-cyan-500 hover:text-indigo-500">Continuer mon marché<span aria-hidden="true"> &rarr;</span></button>
                 </p>
             </div> -->
         </div>
     </div>
     <!-- <a class="text-center" href="/panier">Voir le Panier</a> -->
 </div>
 <script>
     const ulCartListLocal = document.getElementById("ArticleListLi");
     document.getElementById("ArticleListLi").innerHTML = "";
     const total_final = document.getElementById("numberOfArt");
     let localTotal = localStorage.getItem("Total") !== null ? localStorage.getItem("Total") : 0;
     total_final.innerHTML = `${localTotal} €`;

     let localArticlesSho = localStorage.getItem("ArtcilesInCart")
     let allArticlesSho = localArticlesSho !== null && localArticlesSho !== "" ? JSON.parse(localStorage.getItem("ArtcilesInCart")) : [];
     //  let localArray = [];
     //  localArray.push(allArticles)
     allArticlesSho.forEach((element, index) => {
         console.log("One of article", element);
         let li = document.createElement("li");
         li.setAttribute("class", "flex py-6");
         li.setAttribute("id", `${element.cartItemId}`);
         li.innerHTML = `
     <div class="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
         <img src=${element.image} alt="" class="h-full w-full object-cover object-center">
     </div>

     <div class="ml-4 flex flex-1 flex-col">
         <div>
             <div class="flex justify-between text-base font-medium text-gray-900">
                 <h3>
                     <a href="#">${element.name}</a>
                 </h3>
                 <p class="ml-4">${element.price} €</p>
             </div>
             <p class="mt-1 text-sm text-gray-500">${element.description}</p>
         </div>
         <div class="flex flex-1 items-end justify-between text-sm">
            <!-- <p class="text-gray-500">Délicieux</p> -->

             <div class="flex">
                 <button id="${element.cartItemId}" type="button" class="font-medium text-indigo-600 hover:text-indigo-500 removeCart">Retirer</button>
             </div>
         </div>
     </div>
     `;
         ulCartListLocal.appendChild(li);
     });
 </script>
