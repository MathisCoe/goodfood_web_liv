<ul class="stepper" data-mdb-stepper="stepper" data-mdb-stepper-type="vertical">
    <li class="stepper-step ">
        <div class="stepper-head">
            <span class="stepper-head-icon"> 1 </span>
            <span class="stepper-head-text"> Etat : En Cours </span>
        </div>
        <div class="stepper-content">
            T'avais acheté trop de trucs
        </div>
    </li>
    <li class="stepper-step">
        <div class="stepper-head">
            <span class="stepper-head-icon"> 2 </span>
            <span class="stepper-head-text"> Etat : Terminé </span>
        </div>
        <div class="stepper-content">
            T'as suivi un régime ces derniers temps
        </div>
    </li>
    <li class="stepper-step stepper-active">
        <div class="stepper-head">
            <span class="stepper-head-icon"> 3 </span>
            <span class="stepper-head-text"> Etat : Annulé </span>
        </div>
        <div class="stepper-content">
            Tu prépares un marathon apparemment
        </div>
    </li>

</ul>
