@extends('layouts.app')

@section('title', 'Accueil')

@section('sidebar')
@endsection

@section('content')
@include('partials._hero')
<center>
<div id="carouselDarkVariant" class="carousel slide carousel-fade carousel-dark relative w-25" data-bs-ride="carousel">
        <div class="carousel-inner relative w-full overflow-hidden">

            <div class="carousel-item active relative float-left w-full">
                <img src='{{asset("/images/produits/3.png")}}' class="block w-full" alt="Motorbike Smoke" />
            </div>

            <div class="carousel-item relative float-left w-full">
                <img src='{{asset("/images/produits/1.jpg")}}' class="block w-full" alt="Mountaintop" />
        
            </div>
            <div class="carousel-item relative float-left w-full">
                <img src='{{asset("/images/produits/2.jpg")}}' class="block w-full" alt="Woman Reading a Book" />
                
            </div>
        </div>
        <button class="carousel-control-prev absolute top-0 bottom-0 flex items-center justify-center p-0 text-center border-0 hover:outline-none hover:no-underline focus:outline-none focus:no-underline left-0" type="button" data-bs-target="#carouselDarkVariant" data-bs-slide="prev">
            <span class="carousel-control-prev-icon inline-block bg-no-repeat" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next absolute top-0 bottom-0 flex items-center justify-center p-0 text-center border-0 hover:outline-none hover:no-underline focus:outline-none focus:no-underline right-0" type="button" data-bs-target="#carouselDarkVariant" data-bs-slide="next">
            <span class="carousel-control-next-icon inline-block bg-no-repeat" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</center>

@endsection
