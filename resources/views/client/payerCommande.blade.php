@extends('layouts.app')

@section('title', 'Payer')

@section('sidebar')
@endsection
@section('content')
<form action="/payerCommande/{{$idCommande}}/final" method="POST">
    {{csrf_field()}}
    <div class="min-w-screen min-h-screen bg-gray-200 flex items-center justify-center px-5 pb-10 pt-16">
        <div class="w-full mx-auto rounded-lg bg-white shadow-lg p-5 text-gray-700" style="max-width: 600px">
            <div class="w-full pt-1 pb-5">
                <div class="bg-lime-500 text-white overflow-hidden rounded-full w-20 h-20 -mt-16 mx-auto shadow-lg flex justify-center items-center">
                    <i class="fa fa-credit-card"></i>
                </div>
            </div>
            <div class="mb-10">
                <h1 class="text-center font-bold text-xl uppercase">Paiement sécurisé </h1>
            </div>
            <div class="mb-3 flex -mx-2">
                <div class="px-2">
                    <label for="type1" class="flex items-center cursor-pointer">
                        <input type="radio" class="form-radio h-5 w-5 text-indigo-500" name="type" id="type1" checked>
                        <img src='{{asset("/images/credit_cards.png")}}' class="h-8 ml-3">
                    </label>
                </div>
                <div class="px-2">
                    <label for="type2" class="flex items-center cursor-pointer">
                        <input type="radio" class="form-radio h-5 w-5 text-indigo-500" name="type" id="type2">
                        <img src='{{asset("/images/pay_pal.png")}}' class="h-8 ml-3">
                    </label>
                </div>
            </div>
            <h2 id="totalCart" class=" text-base font-medium text-gray-900">Payer la commande d'une valeur de {{$prixTotal}} €</h2> </br>
            <div class="mb-3">
                <label class="font-bold text-sm mb-2 ml-1">Nom sur la carte</label>
                <div>
                    <input class="w-full px-3 py-2 mb-1 border-2 border-gray-200 rounded-md focus:outline-none focus:border-indigo-500 transition-colors" required placeholder="NOM Prénom" type="text" />
                </div>
            </div>
            <div class="mb-3">
                <label class="font-bold text-sm mb-2 ml-1">Numéro de carte</label>
                <div>
                    <input type="text" maxlength="255" name="carte_numero" class="form-control" id="carte_numero" required class="w-full px-3 py-2 mb-1 border-2 border-gray-200 rounded-md focus:outline-none focus:border-indigo-500 transition-colors" placeholder="0000 0000 0000 0000" type="text" />
                </div>
            </div>
            <div class="mb-3 ">
                <label class="font-bold text-sm mb-2 ml-1">Date d'expiration</label>
                <div class="datepicker relative form-floating mb-3">
                <input class="w-full px-3 py-2 mb-1 border-2 border-gray-200 rounded-md focus:outline-none focus:border-indigo-500 transition-colors" required placeholder="MM/AA" type="text" />
                </div>

            </div>
            <div class="mb-10">
                <label class="font-bold text-sm mb-2 ml-1">Code de sécurité</label>
                <div>
                    <input class="w-32 px-3 py-2 mb-1 border-2 border-gray-200 rounded-md focus:outline-none focus:border-indigo-500 transition-colors" placeholder="123" type="text" required/>
                </div>
            </div>
            <div>
                <button id="payCart" class="block w-full max-w-xs mx-auto bg-laravel hover:bg-indigo-700 focus:bg-indigo-700 text-white rounded-lg px-3 py-3 font-semibold" type="submit"><i class="mdi mdi-lock-outline mr-1"></i> Payer {{$prixTotal}} €</button>
            </div>
            <div class="mt-6 border-b border-gray-500 w-full mb-6"></div>
            <div class="mb-3 text-sm ">
                <p class="mb-2 flex items-center justify-center">Ce processus de commande est mené par BNB, qui s’occupe également des demandes aux services à la clientèle et des retours.</p>
                <p class="mb-2 flex items-center justify-center">Vos données seront partagées avec Goodfood Inc pour le traitement de la livraison du produit. Goodfood.com, 15 ou 20 rue de la cigale, LE MANS 72000</p>
                <p class="underline flex items-center justify-center">Politique de protection des données personnelles</p>
                <p class="underline flex items-center justify-center">Cookies | Gérer mes cookies</p>
            </div>
        </div>
    </div>

    <!-- <div class="flex space-x-2 justify-center">
        <h2 id="totalCart" class=" text-base font-medium text-gray-900">Payer la commande</h2> </br>
        <h1>Total : {{$prixTotal}} € </h1>

        <label class="col-form-label" for="modal-input-id">Numéro:</label>
        <input type="text" maxlength="255" name="carte_numero" class="form-control" id="carte_numero" required>
        <label class="col-form-label" for="modal-input-id">Date d'expiration:</label>
        <input type="text" maxlength="255" name="carte_expiration" class="form-control" id="carte_expiration" required>
        <label class="col-form-label" for="modal-input-id">CVC:</label>
        <input type="text" maxlength="255" name="carte_cvc" class="form-control" id="carte_cvc" required>

        <button type="submit" class="block bg-laravel text-white mt-6 py-2 rounded-xl hover:opacity-80"> PAYER </button>
</div> -->
</form>
<script>
    let cleanCartAfterPaid = document.getElementById("payCart");
    if (cleanCartAfterPaid !== null) {
        cleanCartAfterPaid.addEventListener("click", (event) => {
            // localStorage.setItem("ArtcilesInCart", '');
            localStorage.removeItem("ArtcilesInCart");
            // localStorage.setItem("Total", '0');
            localStorage.removeItem("Total");
            console.log("Remove Cart content");
        });
    }
</script>
@endsection
