@extends('layouts.app')

@section('title', 'Panier')

@section('sidebar')
@endsection
@section('content')
<form action="/creerCommande" method="POST">
{{csrf_field()}}
<div class="flex space-x-2 justify-center">
        <h2 id="totalCart" class=" text-base font-medium text-gray-900">PANIER</h2>
        <div>
            <button id="cleanCarts" type="button" class="inline-block rounded-full bg-red-600 text-white leading-normal uppercase shadow-md hover:bg-cyan-300 hover:shadow-lg focus:bg-cyan-300 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out w-9 h-9">
                <i class="fa fa-remove"></i>
            </button>

        </div>
        <div>

                <p id="totalToPassFromLS" style="display: none" name="totalToPay"></p>
                <button type="submit" class="inline-block rounded-full bg-lime-500 text-white leading-normal uppercase shadow-md hover:bg-cyan-300 hover:shadow-lg focus:bg-cyan-300 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out w-9 h-9" type="submit">
                    <i class="fa fa-credit-card"></i>
                </button>

        </div>
    </div>
<div class="mx-4">
    <div id="formCart" class="bg-gray-50 border border-gray-200 p-10 rounded flex justify-center lg:grid lg:grid-cols-3 gap-4 space-y-4 md:space-y-0 mx-4 mt-6 ">
    </div>
</div>
</form>
<script>
    const cartForml = document.getElementById("formCart");
    const totalCart = document.getElementById("totalCart");
    const totalToPassToForm = document.getElementById("totalCart");
    totalToPassToForm.innerHTML = localTotal
    totalCart.innerHTML = `Valeur totale de votre commande ${localTotal} €`;
    let cartContent = JSON.parse(localStorage.getItem("ArtcilesInCart"));
    nbArticle = 1;
    cartContent.forEach((element) => {
        console.log("One of article", element);
        let div = document.createElement("div");
        div.setAttribute("class", "flex justify-center rounded-lg shadow-lg bg-white max-w-sm");
        div.innerHTML = `
        <input type="hidden" name="type[${nbArticle}]" value="${element.type}"></input>
        <input type="hidden" name="id[${nbArticle}]" value="${element.id}"></input>
        <input type="hidden" name="prix[${nbArticle}]" value="${element.price}"></input>

        <a href="#!" data-mdb-ripple="true" data-mdb-ripple-color="light">
            <img class="w-20 mr-6 mt-2 rounded-t-lg" src=${element.image} alt="" />
        </a>
        <div class="p-6">
            <h5 class="text-gray-900 text-xl font-medium mb-2">${element.name}</h5>
            <i class="latin">${element.type}</i>
            <p class="text-gray-700 text-base mb-4">
            ${element.price} €
            </p>
            <p style="display: none"> Enlève le style du tag si tu Tu veux vraiment l'id ds la page (${element.id})</p>
            <!-- <button type="button" class=" inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">Button</button> -->
        </div>
        </div>
         `;
         nbArticle++;
        cartForml.appendChild(div);
    });
</script>
@endsection
