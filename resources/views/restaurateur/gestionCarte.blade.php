 
@extends('layouts.app')
 
 @section('title', 'gestion carte')
  
 @section('sidebar')
 @endsection
 @section('content')
 <center> <h2>VOS MENUS </h2> 
 <button type="button" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50" data-bs-toggle="modal" data-bs-target="#creerMenu">
Créer un menu
</button></center>
 @foreach($menus as $menu)
 <img class="w-20 mr-6 mt-2 rounded-t-lg" src='{{asset("/images/menus/".$menu->urlPhoto)}}' alt="" />
<p>Nom du menu : {{$menu->nomMenu}} </p>
<p> Description : {{$menu->descriptionMenu}} </p>
<p> Prix : {{$menu->prix}} € </p>
<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/changeDispo">
{{csrf_field()}}
<input type="hidden" name="idMenu" value="{{$menu->idMenu}}"></input>
@if($menu->disponibilite == true)
<input type="hidden" name="dispo" value="0"></input>
<button type="submit" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50">Mettre en rupture de stock</button> 
@else
<input type="hidden" name="dispo" value="1"></input>
<button type="submit" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50">Mettre disponible de stock</button> 
@endif
</form>

<a href="" type="button" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50">Modifier le menu</a> 
@endforeach

<center> <h2>VOS PRODUITS </h2>
<button type="button" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50" data-bs-toggle="modal" data-bs-target="#creerProduit">
Créer un produit
</button> </center>

@foreach($categoriesProduits as $categorie)
@if($categorie->isInRestaurant($idRestaurant) == true)
<div class="text-xl font-bold mb-4">{{$categorie->nom}}</div>
  
    @foreach($produits as $produit)
    @if($produit['idCategorieProduit'] == $categorie->idCategorieProduit)
    <img class="w-20 mr-6 mt-2 rounded-t-lg" src='{{asset("/images/produits/".$produit->urlPhoto)}}' alt="" />
<p>Nom du menu : {{$produit->nom}} </p>
<p> Description : {{$produit->description}} </p>
<p> Prix : {{$produit->prix}} € </p>
<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/changeDispo">
{{csrf_field()}}
<input type="hidden" name="idProduit" value="{{$produit->idProduit}}"></input>
@if($produit->disponibilite == true)

<input type="hidden" name="dispo" value="0"></input>
<button type="submit" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50">Mettre en rupture de stock</button> 
@else
<input type="hidden" name="dispo" value="1"></input>
<button type="submit" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50">Mettre disponible de stock</button> 
@endif
</form>
<a href="" type="button" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50">Modifier le produit</a> 
    @endif
    @endforeach
<!-- </ul> -->
@endif
    @endforeach


@endsection

@section('modals')
<!-- Modal créer produit -->
<div class="modal fade" id="creerMenu" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Créer un Menu</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ route('creerMenu') }}">
      {{csrf_field()}}
      <div class="modal-body">
      <div class="form-group">
                <label class="col-form-label" for="modal-input-id">Photo:</label>
                <input type="file"  name="img" id="img_ajout">
              </div>
      <div class="form-group">
        <label class="col-form-label" for="modal-input-id">Nom du menu:</label>
        <input type="text" maxlength="255" name="nom" class="form-control" id="nom" required>
    </div>

    <div class="form-group">
            <label for="description">Description : </label>
          <div id="description">
              <textarea id="description" name="description" class="form-control" rows="5" cols="33"></textarea>
          </div>
        </div>
      </div>
      <div class="form-group">
            <label for="prixDiv">Prix : </label>
          <div id="prixDiv">
          <input id="prix" name="prix" type="number" step="0.01" class="form-control">
          </div>
        </div>

        @foreach($categoriesProduits as $categorie)
        @if($categorie->isInRestaurant($idRestaurant) == true)
          <div class="text-xl font-bold mb-4">{{$categorie->nom}}</div>

          @foreach($produits as $i => $produit)
         
            @if($produit['idCategorieProduit'] == $categorie->idCategorieProduit)
              <input type="checkbox" id="produit" name="produits[{{$i}}]" value="{{$produit->idProduit}}">
              <label for="produit"> {{$produit->nom}}</label><br>
            @endif
      
          @endforeach
          @endif
        @endforeach
   
      <div class="modal-footer">
        <button type="button" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50" data-bs-dismiss="modal">Annuler</button>
        <button type="submit" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50">Sauvegarder</button>
      </div>
</form>
    </div>
  </div>
</div>
<!-- Modal créer produit -->
<div class="modal fade" id="creerProduit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Créer le Produit</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ route('creerProduit') }}">
      {{csrf_field()}}
      <div class="modal-body">
      <div class="form-group">
                <label class="col-form-label" for="modal-input-id">Photo:</label>
                <input type="file"  name="img" id="img_ajout">
              </div>


      <div class="form-group">
        <label class="col-form-label" for="modal-input-id">Nom du produit:</label>
        <input type="text" maxlength="255" name="nom" class="form-control" id="nom" required>
    </div>

    <div class="form-group">
            <label class="col-form-label" for="modal-input-description">Catégorie:</label>
            <select id="idCategorieProduit" name="idCategorieProduit" class="form-control">
            @foreach($categoriesProduits as $categorie)
              <option value="{{$categorie->idCategorieProduit}}">{{$categorie->nom}}</option>
              @endforeach
            </select>
          </div>


    <div class="form-group">
            <label for="description">Description : </label>
          <div id="description">
              <textarea id="description" name="description" class="form-control" rows="5" cols="33"></textarea>
          </div>
        </div>
      </div>
      <div class="form-group">
            <label for="prixDiv">Prix : </label>
          <div id="prixDiv">
          <input id="prix" name="prix" type="number" step="0.01" class="form-control">
          </div>
        </div>
    
   
      <div class="modal-footer">
        <button type="button" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50" data-bs-dismiss="modal">Annuler</button>
        <button type="submit" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50">Sauvegarder</button>
      </div>
</form>
    </div>
  </div>
</div>
@endsection