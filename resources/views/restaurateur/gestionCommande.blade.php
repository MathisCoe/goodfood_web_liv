
@extends('layouts.app')

 @section('title', 'Historique commandes')

 @section('sidebar')
 @endsection
 @section('content')

 <center><h3 class="text-2xl text-gray-700 font-bold mb-6 -ml-3">HISTORIQUE DES COMMANDES</h3></center>
 <ol class=" ml-10 border-l-2 border-red-600">
 @foreach($commandes as $commande)
 <li>
 <div class="md:flex flex-start commande">
            <div class="bg-red-600 w-6 h-6 flex items-center justify-center rounded-full -ml-3">
                <svg aria-hidden="true" focusable="false" data-prefix="fas" class="text-white w-3 h-3" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="currentColor" d="M0 464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V192H0v272zm64-192c0-8.8 7.2-16 16-16h288c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16v-64zM400 64h-48V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48H160V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48H48C21.5 64 0 85.5 0 112v48h448v-48c0-26.5-21.5-48-48-48z"></path>
                </svg>
            </div>
            <div class="block p-6 rounded-lg shadow-lg bg-gray-100 max-w-md ml-6 mb-10">
                <div class="flex justify-between mb-4">
                    <a href="#!" class="font-medium text-neutral-900 hover:text-neutral-900 focus:text-red-600 duration-300 transition ease-in-out text-sm">Commande N°{{$commande->idCommande}}</a> </br>
                    <a href="#!" class="font-medium text-neutral-900 hover:text-neutral-900 focus:text-red-600 duration-300 transition ease-in-out text-sm"> du {{$commande->created_at}}</a>
                </div>
                <a href="#!" class="font-medium text-neutral-900 hover:text-neutral-900 focus:text-red-600 duration-300 transition ease-in-out text-sm"> pour {{$commande->user->prenom}} {{$commande->user->nom}}</a>

 <p> Commande N°{{$commande->idCommande}} du {{$commande->created_at}} pour {{$commande->user->prenom}} {{$commande->user->nom}}</p>
@if($commande->etatCommande == 1)
    <p> Etat : En Cours </p>
@elseif($commande->etatCommande == 2)
    <p> Etat : Terminé </p>
@elseif($commande->etatCommande == 3)
    <p> Etat : Annulé </p>
@elseif($commande->etatCommande == 4)
    <p> Etat : Non payée </p>
@endif

 @foreach($commande->menus() as $menu)
<p>{{$commande->quantiteMenu($menu->idMenu)}} x {{$menu->nomMenu}} </p>
@endforeach

@foreach($commande->produits() as $produit)
<p>{{$commande->quantiteProduit($produit->idProduit)}} x {{$produit->nom}} </p>
@endforeach

@if($commande->etatCommande == 1)
<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ route('terminerCommande') }}">
{{csrf_field()}}
<input type="hidden" name="idCommande" value="{{$commande->idCommande}}"></input>
<button type="submit" class="text-white rounded py-2 mb-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50">Terminer la commande</button>
</form>
<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ route('annulerCommande') }}">
{{csrf_field()}}
<input type="hidden" name="idCommande" value="{{$commande->idCommande}}"></input>
<button type="submit" class="text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50">Annuler la commande</button>
</form>
@endif

</div>
</br>
</li>
@endforeach
</ol>

@endsection
