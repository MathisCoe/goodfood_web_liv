@extends('layouts.app')

@section('title', 'Liste restaurant')

@section('sidebar')
@endsection
@section('content')
@include('partials._hero')
@include('partials._search')
<center>
    @if(\Auth::user())
    <h2>LES RESTAURANTS DE VOTRE DEPARTEMENT </h2>
    @else
    <h2>LES RESTAURANTS </h2>
    @endif
</center>
<div class="lg:grid lg:grid-cols-2 gap-4 space-y-4 md:space-y-0 mx-4">
    @foreach($restaurants as $resto)
    <div class="bg-gray-50 border border-gray-200 rounded p-6">
                    <div class="flex">
                        <img
                            class="hidden w-48 mr-6 md:block"
                            src="images/restaurants/{{$resto['urlPhoto']}}"
                            alt=""
                        />
                        <div>
                            <h3 class="text-2xl">
                                <a href="/restaurant/{{$resto['idRestaurant']}}">{{$resto['nomRestaurant']}}</a>
                            </h3>
                            <div class="text-xl font-bold mb-4">Coordonnées</div>
                            <ul class="flex">
                                <li
                                    class="flex items-center justify-center bg-black text-white rounded-xl py-1 px-3 mr-2 text-xs"
                                >
                                    <a href="#">{{$resto['adresse']}}</a>
                                </li>
                                <li
                                    class="flex items-center justify-center bg-black text-white rounded-xl py-1 px-3 mr-2 text-xs"
                                >
                                    <a href="#">{{$resto['telephone']}}</a>
                                </li>
                            </ul>
                            <div class="text-lg mt-4">
                                <i class="fa-solid fa-location-dot"></i> {{$resto['pays']}}
                            </div>
                        </div>
                    </div>
                </div>
    @endforeach
</div>


@endsection
