@extends('layouts.app')

@section('title', 'Menu restaurant')

@section('sidebar')
@endsection
@section('content')
<a href="/restaurant" class="inline-block text-black ml-4 mb-4"><i class="fa-solid fa-arrow-left"></i> Retour
</a>
<center> <div class="text-xl font-bold mb-8"> {{$restaurant->nomRestaurant}} </div>
@if(\Auth::user())
@if(\Auth::user()->grade == 1 && \Auth::user()->restaurants_idRestaurant != $restaurant->idRestaurant)
<div class="text-xl font-bold mb-8"> <a href="/changeRestaurantFavori/{{$restaurant->idRestaurant}}" class="w-64 text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50">Ajouter comme restaurant favori</a>  </div>
@endif
@endif
</center>
@if(count($menus) != 0)
<div class="text-xl font-bold mb-4">LES MENUS</div>
<div class="mx-4">
    <div class="bg-gray-50 border border-gray-200 p-10 rounded flex justify-center lg:grid lg:grid-cols-3 gap-4 space-y-4 md:space-y-0 mx-4 mt-6 ">
        @foreach($menus as $menu)
        <div class="mx-4">
            <div class="bg-gray-50 border border-gray-200 p-10 rounded">
                <div class="flex flex-col items-center justify-center text-center">
                    <img class="rounded-t-lg oobject-contain h-48 w-96" src='{{asset("/images/menus/".$menu["urlPhoto"])}}' alt="" />

                    <h3 class="text-2xl mb-2">{{$menu['nomMenu']}} </h3>
                    <h3 class="text-2xl mb-2">{{$menu['prix']}}€ </h3>
                    <h3></h3>
                    <p style="display: none">{{$menu['idMenu']}}</p>
                    <p style="display: none">menu</p>
                    <p style="display: none">
                        {{$menu['descriptionMenu']}}
                    </p>
                    @if(\Auth::user())
                    <td>
                        <button type="button" class="w-64 text-white border-b rounded mb-2 py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50 addToCart">Ajouter au panier</button>
                    </td>
                    @endif
                    <div class="border-b border-gray-500 w-full mb-6"></div>
                    <div class="text-lg space-y-6">
                        <p>
                            {{$menu['descriptionMenu']}}
                        </p>
                        <a href="/menu/{{$menu['idMenu']}}" class="w-64 inline-block rounded px-6 py-2.5 bg-laravel text-white font-medium text-xs leading-tight uppercase ">
                            Détail</a>

                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<div class="text-xl font-bold mt-4 mb-4">LES PRODUITS A PART</div>
@endif
@foreach($categoriesProduits as $categorie)
@if($categorie->isInRestaurant($restaurant->idRestaurant) == true)
<div class="text-xl font-bold mb-4">{{$categorie->nom}}</div>
<ul class="flex justify-center">

    @foreach($produits as $produit)
    @if($produit['idCategorieProduit'] == $categorie->idCategorieProduit)
    <li class="flex justify-center flex-col bg-black text-white rounded-xl px-3 py-1 mr-2" style="display:flex; justify-content:center; align-items:center">
        <tr>
            <td>
                <img class="w-20 mb-6 rounded-lg" src='{{asset("/images/produits/".$produit["urlPhoto"])}}' alt=""></img>
            </td>
            <td>
                <p class="text-white">{{$produit['nom']}}</p>
            </td>
            <td>
                <p class="text-white">{{$produit['prix']}}€</p>
                <p class="text-white"> </p>
                <p style="display: none">{{$produit['idProduit']}}</p>
                <p style="display: none">produit</p>
                <p style="display: none">{{$produit['description']}}</p>
            </td>
            @if(\Auth::user())
            @if($produit['disponibilite'] == 0)
            <td> <button type="button" class="w-64 text-white rounded py-2 px-4">En Rupture De Stock</button> </td>
            @else
            <td> <button type="button" class="w-64 text-white rounded py-2 px-4 hover:bg-indigo-500 bg-cyan-500 shadow-lg shadow-cyan-500/50 addToCart">Ajouter au panier</button> </td>
            @endif
            @endif
        </tr>
    </li>
    @endif

    @endforeach
    @endif
</ul>

@endforeach
<a href="mailto:test@test.com" class="block bg-laravel text-white mt-6 py-2 rounded-xl hover:opacity-80" style="margin-right: 40em; margin-left: 40em; text-align:center"><i class="ml-2 fa-solid fa-envelope"></i>
    Contacter le restaurant</a>
<script>

</script>
@endsection
