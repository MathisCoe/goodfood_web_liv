@extends('layouts.base')
@section('content')
<div class="container" style="background-color: white">
    <div class="bg-gray-50 border border-gray-200 p-10 rounded max-w-lg mx-auto mt-24">
        <header class="text-center">
            <h2 class="text-2xl font-bold uppercase mb-1">
                Se connecter
            </h2>
        </header>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row mb-6">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-6">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-6">
                            <div class="col-md-8 offset-md-4">
                                <!-- <button type="submit" class="btn btn-primary"> -->
                                <button class="bg-laravel text-white rounded py-2 px-4 hover:bg-black" id="valider" type="submit">

                                    {{ __('SE CONNECTER') }}
                                </button>
                                <!-- <a class="btn btn-primary" href="{{ route('register') }}">
                                    Inscription
                                </a> -->
                                @if (Route::has('password.email'))
                                    <a class="btn btn-link" href="{{ route('password.email') }}">
                                        {{ __('Mot de passe oublié ?') }}
                                    </a>
                                @endif

                            </div>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
