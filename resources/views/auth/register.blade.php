@extends('layouts.app')
@section('nomPage', "Demande d'inscription")
@section('content')
<div class="container" style="background-color: white">
    <div class="bg-gray-50 border border-gray-200 p-10 rounded max-w-lg mx-auto mt-24">
        <header class="text-center">
            <h2 class="text-2xl font-bold uppercase mb-1">
                S'inscrire
            </h2>
            <p class="mb-4">Créer un compte sur GoodFood</p>
        </header>

        <form action="/register" method="POST">
            {{ csrf_field() }}
            <div id="prenomUtilisateur" class="mb-6">
                <label for="prenomUtilisateur" class="inline-block text-lg mb-2">
                    Prénom
                </label>
                <input type="text" class="border border-gray-200 rounded p-2 w-full" name="prenomUtilisateur" required />
            </div>
            <div id="nomUtilisateur" class="mb-6">
                <label for="nomUtilisateur" class="inline-block text-lg mb-2">Nom : </label>
                <input type="text" name="nomUtilisateur" class="form-control blanc" required>
            </div>

            <div id="mail" class="mb-6">
                <label for="email" class="inline-block text-lg mb-2">Email</label>
                <input type="email" class="border border-gray-200 rounded p-2 w-full" name="email" />
                <!-- Error Example -->
                <!-- <p class="text-red-500 text-xs mt-1">
                    Please enter a valid email
                </p> -->
            </div>

            <div class="form-group mb-6">

                <label class="col-form-label" for="modal-input-id">Mot de passe:</label>
                <input type="password" maxlength="255" name="password" class="form-control blanc" id="password" onkeyup='verifMDP();' required>

            </div>

            <div class="form-group mb-6">
                <label class="col-form-label" for="modal-input-id">Confirmation mot de passe:</label>
                <input type="password" maxlength="255" name="Conf_password" class="form-control blanc" id="Conf_password" onkeyup='verifMDP();' required>
                <span id='message'></span>
            </div>
            <div class="mb-6">
                <span id='message'></span>
            </div>
            <div id="restaurants" class="form-group mb-6">
                <label for="restaurants">Votre restaurant favori :  </label>
                <select id="idRestaurant" name="idRestaurant" class="form-control blanc" required>
                @foreach($restaurants as $resto)
                    <option value="{{ $resto->idRestaurant }}" >{{ $resto->nomRestaurant }}</option>
                @endforeach
                </select>
            </div>
            <div class="mb-6">
                <!-- <button
                                type="submit"
                                class="bg-laravel text-white rounded py-2 px-4 hover:bg-black"
                            >
                            S'inscrire
                            </button> -->
                <button class="bg-laravel text-white rounded py-2 px-4 hover:bg-black" id="valider" style="display: none" type="submit">S'inscrire</button>
            </div>

            <div class="mt-8">
                <p>
                    Vous avez déjà un compte?
                    <a href="/login" class="text-laravel">Connecter</a>
                </p>
            </div>
        </form>
    </div>

</div>
<script>
    var verifMDP = function() {
        spanMessage = document.getElementById('message');
        nouveauMdp = document.getElementById('password');
        Confnouveau_mdp = document.getElementById('Conf_password');
        boutonValider = document.getElementById('valider');
        if (nouveauMdp.value == Confnouveau_mdp.value && nouveauMdp.value.length != 0 && Confnouveau_mdp.value.length != 0) {
            spanMessage.style.color = 'green';
            spanMessage.innerHTML = 'OK';
            boutonValider.style.display = 'block';
        } else if (nouveauMdp.value != Confnouveau_mdp.value) {
            spanMessage.style.color = 'red';
            spanMessage.innerHTML = 'Ne correspond pas';
            boutonValider.style.display = 'none';
        } else {
            spanMessage.innerHTML = '';
        }

    }
</script>
@endsection
