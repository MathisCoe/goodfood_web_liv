@extends('layouts.app')

@section('title', 'Menu ')

@section('sidebar')
@endsection
@section('content')

<a href="{{ URL::previous() }}" class="inline-block text-black ml-4 mb-4"><i class="fa-solid fa-arrow-left"></i> Retour
</a>
<center>
    <div id="menu">


        <h2>Menu {{$menu->nomMenu}} </h2>
        <h3>Prix: {{$menu->prix}} €</h3>
</center>




<center>
    <h2>COMPREND </h2>
@foreach($categoriesProduits as $categorie)
@if($categorie->isInMenu($menu->idMenu) == true)
<div class="text-xl font-bold mb-4">{{$categorie->nom}}</div>
<ul class="flex justify-center">

    @foreach($produits as $produit)
    @if($produit['idCategorieProduit'] == $categorie->idCategorieProduit)
    <div class="flex justify-center rounded-lg shadow-lg bg-white max-w-sm">
        <a href="#!" data-mdb-ripple="true" data-mdb-ripple-color="light">
            <img class="w-20 mr-6 mt-2 rounded-t-lg" src='{{asset("/images/produits/".$produit["urlPhoto"])}}' alt="" />
        </a>
        <div class="p-6">
            <h5 class="text-gray-900 text-xl font-medium mb-2">{{$produit['nom']}}</h5>
    </div>
    </div>
    @endif
 
    @endforeach
    @endif
</ul>

@endforeach
</center>
</div>
@endsection
